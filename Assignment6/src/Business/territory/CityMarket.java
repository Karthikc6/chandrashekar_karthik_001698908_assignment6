/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.territory;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class CityMarket {
    private String marketName;
    private MarketManager marketManager;
    private Channel channel;

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public MarketManager getManager() {
        return marketManager;
    }

    public void setManager(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
    
    
}
