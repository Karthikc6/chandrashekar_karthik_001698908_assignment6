/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.territory;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Territory {
    private String name;
    private ArrayList<Market> marketList;

    public Territory() {
        marketList = new ArrayList<Market>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Market> getMarketList() {
        return marketList;
    }

    public void setMarketList(ArrayList<Market> marketList) {
        this.marketList = marketList;
    }
    
    
}
