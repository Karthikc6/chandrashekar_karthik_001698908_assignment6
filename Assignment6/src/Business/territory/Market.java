/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.territory;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class Market {
    private String marketName;
    private MarketManager marketManager;
    private Channel channel;
    private ArrayList<SubMarket> subMarketList;

    public Market() {
        subMarketList = new ArrayList<SubMarket>();
    }
    
    public ArrayList<SubMarket> getSubMarketList() {
        return subMarketList;
    }

    public void setSubMarketList(ArrayList<SubMarket> subMarketList) {
        this.subMarketList = subMarketList;
    }
    
    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public MarketManager getMarketManager() {
        return marketManager;
    }

    public void setMarketManager(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
    
    public SubMarket addSubMarket() {
        SubMarket subMarket = new SubMarket();
        subMarketList.add(subMarket);
        return subMarket;
    }
    
    
}
