/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.territory;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class StateMarket{
    private String marketName;
    private MarketManager marketManager;
    private Channel channel;
    private ArrayList<CityMarket> cityMarketList;

    public StateMarket() {
        cityMarketList = new ArrayList<CityMarket>();
    }

    public ArrayList<CityMarket> getCityMarketList() {
        return cityMarketList;
    }

    public void setCityMarketList(ArrayList<CityMarket> cityMarketList) {
        this.cityMarketList = cityMarketList;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public MarketManager getMarketManager() {
        return marketManager;
    }

    public void setMarketManager(MarketManager marketManager) {
        this.marketManager = marketManager;
    }


    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
    
    public CityMarket addCityMarket() {
        CityMarket cityMarket = new CityMarket();
        cityMarketList.add(cityMarket);
        return cityMarket;
    }
}
