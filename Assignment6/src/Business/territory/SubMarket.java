/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.territory;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class SubMarket {
    private String marketName;
    private MarketManager marketManager;
    private Channel channel;
    private ArrayList<StateMarket> stateMarketList;
    
    public SubMarket() {
        stateMarketList = new ArrayList<StateMarket>();
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public MarketManager getMarketManager() {
        return marketManager;
    }

    public void setMarketManager(MarketManager marketManager) {
        this.marketManager = marketManager;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public ArrayList<StateMarket> getStateMarketList() {
        return stateMarketList;
    }

    public void setStateMarketList(ArrayList<StateMarket> stateMarketList) {
        this.stateMarketList = stateMarketList;
    }
    
    public StateMarket addStateMarket() {
        StateMarket stateMarket = new StateMarket();
        stateMarketList.add(stateMarket);
        return stateMarket;
    }
    
}
