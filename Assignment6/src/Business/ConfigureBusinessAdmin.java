/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Trupti
 */
public class ConfigureBusinessAdmin {
    public static Business initializeBusiness()
    {
    Business business = Business.getInstance();
    //Create admin as Employee
    
    Employee employee = business.getEmployeeDirectory().addEmployee();
    employee.setFirstName("Admin");
    employee.setLastName("");
    employee.setOrganization("NEU");
    
    // Create useraccount for admin
    
    UserAccount userAccount = business.getUserAccountDirectory().addUserAccount();
    userAccount.setUserName("Admin");
    userAccount.setPassword("admin123");
    userAccount.setPerson(employee);
    userAccount.setRole(UserAccount.ADMIN_ROLE);
    userAccount.setIsActive(true);
    return business;
    }
}   
