/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Trupti
 */
public class UserAccount {
  
    public static String ADMIN_ROLE = "admin";
    public static String EMPLOYEE_ROLE = "supplier";
    public static String CUSTOMER_ROLE = "customer";
    public static String TERRITORY_MANAGER_ROLE = "territory manager";
    public static String STATE_MANAGER_ROLE = "state manager";
    public static String ZONE_MANAGER_ROLE = "zone manager";
    public static String CITY_MANAGER_ROLE = "city manager";
    private static int count = 1000;

    private int userId;
    private String firstName;
    private String lastName;
    private String city;
    private String country;
    private String zone;
    private String age;
    private String state;
    private String userName;
    private String password;
    private String role;
    private boolean isActive;
    private Person person;
    private String profession;

    public String getPofession() {
        return profession;
    }

    public void setPofession(String pofession) {
        this.profession = pofession;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        UserAccount.count = count;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getUserId() {
        return userId;
    }

    public UserAccount() {
        userId = count++;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    @Override
    public String toString()
            {
 return person.getFirstName()+""+ person.getLastName();     
            }
}
